# Simon BR
Para mais informações, consulte o site: http://www.simon-listens.org/index.php?id=38&L=3

Há também vídeos de 2010/2011 sobre a aplicação:   
- [SimonBR - Programa Sementes da TV Cultura](https://www.youtube.com/watch?v=dVnB_CH7Sx0)    
- [SimonBR - Demonstração por Patrick Alves (ex LaPS)](https://www.youtube.com/watch?v=QCRN8y7Y7OI)

# Copyright original
Universidade Federal do Pará (UFPA)     
Laboratório de Processamento de Sinais (LaPS)    
Grupo FalaBrasil 04-10-2010                   

# Detalhes do modelo acústico
LapsAM-1.5-Simon: modelo acústico para o Português Brasileiro.
- 39 fones
- 59.321 trifones lógicos e 8.997 físicos
- Trifones do tipo cross-word
- 16 gaussianas por mistura
- 15h e 41 minutos de áudio a 16 kHz
- Tipo paramétrico: MFCC\_E\_D\_A\_Z
- Dicionário utilizado: UFPAdic3.0

# NOTAS: Feb 2019
- Grupo Falabrasil agora é parte do Laboratório de Visualização, Interação e
  Sistemas Inteligentes (LabVIS)    
- O antigo LaPS tornou-se em boa parte LASSE - Núcleo de Pesquisa e
  Desenvolvimento em Telecomunicações, Automação e Eletrônica   
- O projeto SimonBR foi descontinuado e as versões dos cenários e dos modelos
  acústicos podem não funcionar em versões mais atuais. O último teste com a
  plataforma foi realizado em 2015.

__Grupo FalaBrasil (2019)__     
__Universidade Federal do Pará__    
